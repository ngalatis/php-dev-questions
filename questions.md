# Questions

_Refer to README.md for further instructions._

1. **Explain the different types of errors in PHP**
2. **How can we know the number of days between two given dates using PHP?**
3. **What is difference between require_once(), require(), include()?**
4. **What is the difference between session_unregister() and session_unset()**
5. **How do I escape data before storing it in the database?**
6. **How can we get the IP address of the client?**
7. **What is the difference between GET and POST?**
8. **What are the __construct() and __destruct() methods in a PHP class?**
9. **The value of the variable input is a string 1,2,3,4,5,6,7. How would you get the sum of the integers contained inside input?**
10. **In Drupal 8 .theme file you have this:**
```
function THEME_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    
    $title = \Drupal::routeMatch()->getParameter('node')->getTitle();
    $contType = \Drupal::routeMatch()->getParameter('node')->getType();
    $title = strtolower(str_replace(' ','_',$title));

    if ($contType !== 'article') { 

      array_splice($suggestions, 3, 0, 'node__' . $title);

    }
  }
}


function THEME_theme_suggestions_node_alter(array &$suggestions, array $variables) {
  
  if ($node == \Drupal::routeMatch()->getParameter('node')) {
    
    $title = \Drupal::routeMatch()->getParameter('node')->getTitle();
    $contType = \Drupal::routeMatch()->getParameter('node')->getType();
    $title = strtolower(str_replace(' ','_',$title));

    if ($contType !== 'article') { 

      array_splice($suggestions, 3, 0, 'node__' . $title);

    }
  }
}
```
One of them is wrong. Find it, explain why, then refactor the code to be more efficient.
